import time
import csv
import sys
import requests
import base64
from bs4 import BeautifulSoup
from progressbar import ProgressBar, Bar, ETA, ReverseBar, FileTransferSpeed, RotatingMarker, Percentage

reload(sys)
sys.setdefaultencoding('utf8')
sys.getdefaultencoding()
timestr = time.strftime("%Y%m%d")
ENTFERNUNG = 50
STADT = "aschaffenburg"
BRANCHE = "bauunternehmen"
NO_EMAIL_PLACEHOLDER = "keine_email_vorhanden@keingluck.ba"
NAME_SEARCH = '"span", attrs={"itemprop": "name"}'
SAVED_COSTUMERS = 0
print "Today is the: "+timestr


def check_distance(distance):
    if distance > 0:
        city_range = ",,,,,umkreis-" + str(ENTFERNUNG) + "000/"
    else:
        city_range = "/"
    return city_range
UMKREIS = check_distance(ENTFERNUNG)
URL = 'http://www.gelbeseiten.de/' + BRANCHE + '/' + STADT + UMKREIS


class CrazyFileTransferSpeed(FileTransferSpeed):
    "It's bigger between 45 and 80 percent"

    def update(self, pbar):
        if 45 < pbar.percentage() < 80:
            return 'Bigger Now ' + FileTransferSpeed.update(self, pbar)
        else:
            return FileTransferSpeed.update(self, pbar)


def find_results(url):
    """get all results string"""
    # print "finding results on site"
    html = requests.get(URL)
    soup = BeautifulSoup(html.text, 'lxml')
    results = soup.find("span", class_="gs_titel_anzahlTreffer")
    results_int = int(results.text)
    pages = results_int / 15
    modulus = results_int % 15
    if modulus >= 1:
        pages += 1
    print "found :" + results.text + " results"
    # print "finished"
    results_text = results.text
    return pages, results_int


def create_pages(number):
    """Create results/15 number of pages"""
    page_list = []
    # print "creating page links"
    for i in range(number):
        page_list.extend([URL + "s" + str(i + 1) + "/branche"])

    return page_list


def get_customers(page):
    """returns a list of customers"""
    customers_links = []
    html = requests.get(page)
    soup = BeautifulSoup(html.text, 'lxml')
    # print "geting customers"
    customers_on_page = soup.find_all("a", class_="m74_mehr_details_button")
    customers_links = ([e.attrs.get("href") for e in customers_on_page])

    return customers_links


def write_first_row():
    """writing firstrow to csv"""
    with open(BRANCHE + STADT + timestr + ".csv", "w") as leads_csv:
        writer = csv.writer(leads_csv, delimiter=";")
        writer.writerow(['EMAIL',
                         'WEBSITE',
                         'NAME',
                         'ADDRESS',
                         'PLZ',
                         'CITY',
                         'PHONE'])
    return 0


def write_to_csv(data_list):
    """writing data to csv"""
    with open(BRANCHE + STADT + timestr + ".csv", "a") as leads_csv:
        writer = csv.writer(leads_csv, delimiter=";")
        for customer in data_list:
            writer.writerow(customer)

    return 0


def b64_encode(suffix):
    """encode"""
    encoded_string = base64.b64decode(suffix)
    return encoded_string


def is_there_a_mail(soup):
    email_counter = 0
    website_counter = 0
    website_found = "No Website Found"
    email_found = "No Mail"
    find_mail = soup.find_all(itemprop="email")
    if len(find_mail) > 0:
        email_found = find_mail[0].string
        email_counter += 1
    else:
        return email_counter, website_counter, email_found, website_found

    find_website = soup.find(rel="follow")
    if len(find_website.contents) == 5:
        website_found = find_website.contents[3].string
        website_counter += 1
    else:
        website_found = "No Website Found"

    return email_counter, website_counter, email_found, website_found


def get_number_ganz(numbers):
    n_list = []
    n_finish_list = []
    for number in numbers:
        if len(number.attrs[u'class']) == 3:
            what_kind_of_number = "Fax"
        else:
            what_kind_of_number = "Telefon"
        if number.contents[1].string is None:
            num = ""
        else:
            num = number.contents[1].string
        suffix = number.contents[3].attrs[u'data-telsuffix']
        number_ganz = num + b64_encode(suffix)
        n_list.append([what_kind_of_number, number_ganz])
    if len(n_list) > 1 and n_list[0] == n_list[1]:
        n_list.pop(1)
    for i in range(len(n_list)):
        number_finish = n_list[i][0] + ": " + str(n_list[i][1])
        n_finish_list.append(number_finish)

    return n_finish_list


def get_name_address_and_number(soup):
    name = str(soup.find(itemprop="name").text)
    try:
        address = str(soup.find(itemprop="streetAddress").text)
    except AttributeError:
        address = "No Address"
    postal_code = str(soup.find(itemprop="postalCode").text)
    city = str(soup.find(itemprop="addressLocality").text)
    numbers = get_number_ganz(soup.find_all(class_="nummer_ganz"))

    return name, address, postal_code, city, numbers

def get_data_from(customers, page_number, page_counter):
    """get all data"""
    pbc = 1
    widget_csv_progress = ['Geting Data: ' + str(page_counter) + '. Page', ' ', Bar(marker="#"), ' ', Percentage()]
    data_progress = ProgressBar(widgets=widget_csv_progress, maxval=16).start()

    data = []
    email_counter = 0
    website_counter = 0

    for customer in customers:
        data_progress.update(pbc)
        page = requests.get(customer)
        soup = BeautifulSoup(page.content, 'html.parser')
        check = is_there_a_mail(soup)
        pbc += 1
        if check[0] == 1:         #if no mail is found return to main
            name_address_number = get_name_address_and_number(soup)
            mail_and_webasite = (str(check[2]), str(check[3]))
            sve = mail_and_webasite + name_address_number
            data.append(sve)

            #print name_address_number[0]
            email_counter += check[0]
            website_counter += check[1]

    data_progress.finish()
    print

    return email_counter, website_counter, data


def main():
    """main entry point"""
    write_first_row()
    email_counter = 0
    website_counter = 0
    page_counter = 1
    results = find_results(URL)  # ukupno rezultata
    page_number = results[0]
    print "Pages to get: " + str(page_number)
    pages = create_pages(results[0])  # generates all pages to scrap...example 2 pages with 30 Customers together
    for i, page in enumerate(pages):
        customers_on_page = get_customers(pages[i])     #get links from page[i]

        data = get_data_from(customers_on_page, page_number, page_counter)
        page_counter += 1
        email_counter += data[0]
        website_counter += data[1]
        write_to_csv(data[2])

    return


if __name__ == '__main__':
    sys.exit(main())
