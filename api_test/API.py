import time
import csv
import sys
import requests
import base64
from bs4 import BeautifulSoup
from progressbar import ProgressBar, Bar, ETA, ReverseBar, FileTransferSpeed, RotatingMarker, Percentage

reload(sys)



sys.setdefaultencoding('utf8')
sys.getdefaultencoding()
timestr = time.strftime("%Y%m%d")
print "Today is the: "+timestr
ENTFERNUNG = 50


def check_distance(distance):
    if distance > 0:
        city_range = ",,,,,umkreis-" + str(ENTFERNUNG) + "000/"
    else:
        city_range = "/"
    return city_range


class CrazyFileTransferSpeed(FileTransferSpeed):
    "It's bigger between 45 and 80 percent"

    def update(self, pbar):
        if 45 < pbar.percentage() < 80:
            return 'Bigger Now ' + FileTransferSpeed.update(self, pbar)
        else:
            return FileTransferSpeed.update(self, pbar)


STADT = "aschaffenburg"
BRANCHE = "baeckerei"
UMKREIS = check_distance(ENTFERNUNG)
URL = 'http://www.gelbeseiten.de/' + BRANCHE + '/' + STADT + UMKREIS
NO_EMAIL_PLACEHOLDER = "keine_email_vorhanden@keingluck.ba"
NAME_SEARCH = '"span", attrs={"itemprop": "name"}'
SAVED_COSTUMERS = 0




def show_progress_bars(selection):
    if "m_p_b" in selection:
        widgets_o_p = [Bar('>'), '(:)', ReverseBar('<'), ' ', 'Working']
        overal_progress = ProgressBar(widgets=widgets_o_p, maxval=100).start()
        overal_progress.update(1)
        overal_progress.finish()
    elif "s_p_b":
        widget_csv_progress = [FileTransferSpeed(), ' ', Bar(marker=RotatingMarker()), ' ', 'Saving CSV: ',
                               Percentage()]
        csv_progress = ProgressBar(widgets=widget_csv_progress, maxval=100).start()
        csv_progress.update(1 / 100)
        csv_progress.finish()

    elif "c_p_b":
        widget_csv_progress = [FileTransferSpeed(), ' ', Bar(marker=RotatingMarker()), ' ', 'Saving CSV: ',
                               Percentage()]
        csv_progress = ProgressBar(widgets=widget_csv_progress, maxval=100).start()
        csv_progress.update(1 / 100)
        csv_progress.finish()


def find_results(url):
    """get all results string"""
    # print "finding results on site"
    html = requests.get(URL)
    soup = BeautifulSoup(html.text, 'lxml')
    results = soup.find("span", class_="gs_titel_anzahlTreffer")
    results_int = int(results.text)
    pages = results_int / 15
    modulus = results_int % 15
    if modulus >= 1:
        pages += 1

    print "found :" + results.text + " results"
    # print "finished"

    return pages
def count_emails_and_websites(keyword):
    email_counter = 0
    website_counter = 0
    if keyword.find ("e") == 0:
        email_counter += 1
    elif keyword.find("w") == 0:
        website_counter += 1

    return email_counter, website_counter

def check_e_data(email2check, website2check, email_counter, website_counter):
    try:
        email = email2check
        if email.find("E-Mail schreiben") == 0:
            email = NO_EMAIL_PLACEHOLDER
        elif "http://" in email or "www." in email:
            email = NO_EMAIL_PLACEHOLDER
        elif "@" in email:
            response = count_emails_and_websites("e")
            #print type(response[0])
            if response[0] == 1:
                email_counter += 1
        else:
            email = NO_EMAIL_PLACEHOLDER
    except SystemError:
        pass
    try:
        website = website2check.attrs['href']
        if "http://" in website or "www." in website:
            response = count_emails_and_websites("w")
            if response[1] == 1:
                #print type (response[1])
                website_counter += 1
        else:
            website = "www.keine_website.de"
    except TypeError:
        pass

    return email, website, email_counter, website_counter

def create_pages(number):
    """Create results/15 number of pages"""
    page_list = []
    # print "creating page links"
    for i in range(number):
        page_list.extend([URL + "s" + str(i + 1) + "/branche"])

    return page_list


def get_customers(page):
    """returns a list of customers"""
    customers_links = []
    html = requests.get(page)
    soup = BeautifulSoup(html.text, 'lxml')
    # print "geting customers"
    customers_on_page = soup.find_all("a", class_="m74_mehr_details_button")
    customers_links = ([e.get('href') for e in customers_on_page])

    return customers_links


def get_data_from(customers, email_counter, website_counter):
    """get all data"""
    data_list = []
    #pbw = website_counter
    indexing = 0
    for customer in customers:

        page = requests.get(customer)
        soup = BeautifulSoup(page.text, 'lxml')

        find_name = soup.find_all('span', attrs={"itemprop": "name"})
        find_street = soup.find('span', attrs={"itemprop": "streetAddress"})
        find_postal_code = soup.find_all('span', attrs={"itemprop": "postalCode"})
        find_city = soup.find('span', attrs={"itemprop": "addressLocality"})
        find_number = soup.find("span", class_="nummer")
        find_suffix = soup.find('span', class_="suffix")
        try:
            number_b64 = find_suffix.get("data-telsuffix")
        except AttributeError:
            number_b64 = "nnnn"

        find_data = soup.find_all("span", class_="text")
        find_website = soup.find_all("a", class_="link")

        number_encoded = b64_encode(number_b64)

        name = find_name[0].text

        try:
            street = find_street.text
        except AttributeError:
            street = "Keine Addresse"
        number = find_number.text
        postal_code = find_postal_code[0].text
        city = find_city.text
        email2check = find_data[3].string
        website2check = find_data[4].contents[0]
        e_data = check_e_data(email2check, website2check, email_counter, website_counter)
        email = e_data[0]
        website = e_data[1]
        email_counter = e_data[2]
        website_counter = e_data[3]


        widgets_customer_progress = ['', Bar (marker='#'), ' ', 'Emails: '+str(email_counter), '  ',
                                     'websites:' + str(website_counter)]
        customer_progress = ProgressBar (widgets=widgets_customer_progress, maxval=len (customers)).start ()
        last_name = BRANCHE
        gender = 2
        mail_type = 1
        title = "firma"

        customer_progress.update(indexing)

        indexing += 1
        data_list.append([name,
                          email,
                          street,
                          number + number_encoded,
                          postal_code,
                          city,
                          website,
                          gender,
                          last_name,
                          mail_type,
                          title])


        # print "saved "+str(SAVED_COSTUMERS + 1) + ". Customer data_list"
    customer_progress.finish()
    write_to_csv(data_list)
    #pbw += website_counter_response
    #pbe += email_counter_response

    return email_counter, website_counter


def write_to_csv(data_list):
    """writing data to csv"""
    with open(BRANCHE + STADT + timestr + ".csv", "w") as leads_csv:
        writer = csv.writer(leads_csv, delimiter=";")
        for customer in data_list:
            writer.writerow(customer)

    return 0


def b64_encode(suffix):
    """encode"""
    encode = base64.b64decode(suffix)

    return encode


def main():
    """main entry point"""
    EMAILS_COUNTER = 0
    WEBSITES_COUNTER = 0
    number = find_results(URL)  # ukupno rezultata
    pages = create_pages(number)  # generates all pages to scrap...example 2 pages with 30 Customers together
    number_of_pages = len(pages)
    # print number_of_pages
    customer_data = []
    #print

    for i in range(number_of_pages):
        customers_on_page = get_customers(pages[i])     #get links from page[i]
        get_respones_and_data = get_data_from(customers_on_page, EMAILS_COUNTER, WEBSITES_COUNTER)
        EMAILS_COUNTER += get_respones_and_data[0]
        WEBSITES_COUNTER += get_respones_and_data[1]
        #print "email_counter response check:"
        #print EMAILS_COUNTER
        #print "website_counter response check:"
        #print WEBSITES_COUNTER
    #write_to_csv(get_respones_and_data[0])  #get data for every Customer on page [i]
    return


if __name__ == '__main__':
    sys.exit(main())
