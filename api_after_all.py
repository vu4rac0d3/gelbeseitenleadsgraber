import csv
import requests
import base64
from bs4 import BeautifulSoup
import sys

reload(sys)
sys.setdefaultencoding('utf8')
#da probam izbrisat
BRANCHE = "arztpraxis"
STADT = "aschaffenburg"
UMKREIS = ",,,,,umkreis-50000/"
#UMKREIS = "/"
URL = 'http://www.gelbeseiten.de/' + BRANCHE + '/' + STADT + UMKREIS


def find_results(url):
    print sys.getdefaultencoding()
    """get all results string"""
    print "finding results on site"
    html = requests.get(URL)
    soup = BeautifulSoup(html.text, 'lxml')
    results = soup.find("span", class_="gs_titel_anzahlTreffer")
    results_int = int(results.text)
    pages = results_int / 15
    modulus = results_int % 15
    if modulus >= 1:
        pages += 1

    print "found :" + results.text + " results"
    print "finished"

    return pages


def create_pages(pages2make):
    """Create results/15 number of pages"""
    page_list = []
    print "creating page links"
    for i in range(pages2make):
        page_list.extend([URL + "s" + str(i + 1) + "/branche"])
    print "created : " + str(len(page_list)) + " links"

    return page_list


def get_customers_link(page_links):
    """returns a list of customers"""
    customers_links = []
    print "geting every customers link"
    for url in page_links:  # [:1]:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'lxml')
        customer_on_page = soup.find_all("a", class_="m74_mehr_details_button")
        customers_links.append([e.get('href') for e in customer_on_page])
        print customers_links

    print "finished"

    return customers_links


def open_pages(pages):
    """open pages and get single links"""
    customer_data_list = []
    print "opening every page"
    for single_page in pages:  # [:1]:
        for link in single_page:  # [4:8]:
            print "getting data from: " + link
            customer_data_list.extend([get_customer_data(link)])

    return customer_data_list


def write_to_csv(data_list):
    with open(BRANCHE + ".csv", "w") as leads_csv:
        writer = csv.writer(leads_csv, delimiter=";")
        for customers in data_list:
            writer.writerow(customers)

    return 0


def get_customer_data(url):
    """get all data"""
    customer_data_list = []
    count_emails = 0
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'lxml')
    find_name = soup.find_all('span', attrs={"itemprop": "name"})
    find_street = soup.find('span', attrs={"itemprop": "streetAddress"})
    find_postal_code = soup.find_all('span', attrs={"itemprop": "postalCode"})
    find_city = soup.find('span', attrs={"itemprop": "addressLocality"})
    find_number = soup.find("span", class_="nummer")
    find_suffix = soup.find('span', class_="suffix")
    try:
        number_b64 = find_suffix.get("data-telsuffix")
    except AttributeError:
        number_b64 = "nnnn"

    find_data = soup.find_all("span", class_="text")
    find_website = soup.find_all("a", class_="link")

    number_encoded = b64_encode(number_b64)

    name = find_name[0].text
    try:
        street = find_street.text
    except AttributeError:
        street = "Keine Addresse"
    number = find_number.text
    postal_code = find_postal_code[0].text
    city = find_city.text
    email = find_data[3].text

    try:
        if email.find("E-Mail schreiben") == 0:
            email = "mail@KeineMail.com"
        else:
            count_emails = count_emails + 1
    except SystemError:
        pass

    website = find_website[4].get('title')
    last_name = BRANCHE
    gender = 2
    mail_type = 1
    title = "firma"
    customer_data_list.extend([name,
                               email,
                               street,
                               number + number_encoded,
                               postal_code,
                               city,
                               website,
                               gender,
                               last_name,
                               mail_type,
                               title])
    print "saved in customer_data_list"
    return customer_data_list


def b64_encode(suffix):
    """encode"""
    encode = base64.b64decode(suffix)

    return encode


def main():
    """main entry point"""
    results = find_results(URL)
    page_links = create_pages(results)
    customer_links = get_customers_link(page_links)
    all_data = open_pages(customer_links)
    print "finished with geting"
    write_to_csv(all_data)

    return 0


if __name__ == '__main__':
    sys.exit(main())
