# README #
###
```

                                   #**************************************************************#
                                   #                 /   |                  |  _  |    | ||____ | #
                                   # __   __ _   _  / /| | _ __  __ _   ___ | |/' |  __| |    / / #
                                   # \ \ / /| | | |/ /_| ||'___|/ _` | / __||  /| | / _` |    \ \ #
                                   #  \ V / | |_| |\___  || |  | (_| || (__ \ |_/ /| (_| |.___/ / #
                                   #   \_/   \__,_|   |_/ |_|   \__,_| \___| \___/  \__,_|\____/  #
                                   #______________________________________________________________#

```
###
### What is this repository for? ###

* This python Script is writen to automate the process of retrieving contact information for local buisenesses, the so known Leads, from Germany's Yellow Pages site [Gelbe Seiten](http://www.gelbeseiten.de/).

* Version 2


### How do I get set up? ###
* just run the following command in your "terminal" or "command prompt" if you are on Windows and follow the instructions.

```
#!terminal
user@localhost:~$ python GSLeadsGraber.py

```

### Who do I talk to? ###

* If you have a question feel free to message me at [vu4rac0d3@gmail.com](mailto:vu4rac0d3@gmail.com)