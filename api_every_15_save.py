import time
import csv
import sys
import requests
import base64
from bs4 import BeautifulSoup
from datetime import datetime
from progressbar import ProgressBar, Bar, ETA, ReverseBar

reload(sys)

sys.setdefaultencoding('utf8')
sys.getdefaultencoding()

ENTFERNUNG = 0


def check_distance(distance):
    if distance > 0:
        city_range = ",,,,,umkreis-" + str(ENTFERNUNG) + "000/"
    else:
        city_range = "/"
    return city_range


STADT = "aschaffenburg"
BRANCHE = "baeckerei"
UMKREIS = check_distance(ENTFERNUNG)
URL = 'http://www.gelbeseiten.de/' + BRANCHE + '/' + STADT + UMKREIS
NO_EMAIL_PLACEHOLDER = "keine_email_vorhanden@keingluck.ba"
NAME_SEARCH = '"span", attrs={"itemprop": "name"}'
EMAILS_FOUND = 0
SAVED_COSTUMERS = 0





def find_results(url):
    """get all results string"""
    # print "finding results on site"
    html = requests.get(URL)
    soup = BeautifulSoup(html.text, 'lxml')
    results = soup.find("span", class_="gs_titel_anzahlTreffer")
    results_int = int(results.text)
    pages = results_int / 15
    modulus = results_int % 15
    if modulus >= 1:
        pages += 1

    # print "found :" + results.text + " results"
    # print "finished"

    return pages


def create_pages(number):
    """Create results/15 number of pages"""
    page_list = []
    # print "creating page links"
    for i in range(number):
        page_list.extend([URL + "s" + str(i + 1) + "/branche"])

    return page_list


def get_customers(page):
    """returns a list of customers"""
    customers_links = []
    html = requests.get(page)
    soup = BeautifulSoup(html.text, 'lxml')
    # print "geting customers"
    customers_on_page = soup.find_all("a", class_="m74_mehr_details_button")
    customers_links = ([e.get('href') for e in customers_on_page])

    return customers_links


def get_data_from(customers):
    """get all data"""
    data_list = []
    for i in range(len(customers)):
        widgets_customer_progress = ['Customer :' + str(i + 1), '  ', Bar(marker='#', left='[', right=']'), '  ', ETA()]
        print
        customer_progress = ProgressBar(widgets=widgets_customer_progress, maxval=len(customers)).start()
        page = requests.get(customers[i])
        soup = BeautifulSoup(page.text, 'lxml')
        find_name = soup.find_all('span', attrs={"itemprop": "name"})

        find_street = soup.find('span', attrs={"itemprop": "streetAddress"})
        find_postal_code = soup.find_all('span', attrs={"itemprop": "postalCode"})
        find_city = soup.find('span', attrs={"itemprop": "addressLocality"})
        find_number = soup.find("span", class_="nummer")
        find_suffix = soup.find('span', class_="suffix")
        try:
            number_b64 = find_suffix.get("data-telsuffix")
        except AttributeError:
            number_b64 = "nnnn"

        find_data = soup.find_all("span", class_="text")
        find_website = soup.find_all("a", class_="link")

        number_encoded = b64_encode(number_b64)

        name = find_name[0].text

        try:
            street = find_street.text
        except AttributeError:
            street = "Keine Addresse"
        number = find_number.text
        postal_code = find_postal_code[0].text
        city = find_city.text
        email = find_data[3].text

        try:
            if email.find("E-Mail schreiben") == 0:
                email = NO_EMAIL_PLACEHOLDER
            elif "http://" in email or "www." in email:
                webiste_no_email = email
                email = NO_EMAIL_PLACEHOLDER
            elif "@" in email:
                pass
                # print "Found Email"
            else:
                email = NO_EMAIL_PLACEHOLDER
        except SystemError:
            pass

        try:
            website = find_website[4].get('title')
            if "http://" in website or "www." in website:
                pass
            else:
                website = webiste_no_email
        except TypeError:
            website = "www.keine_website.de"

        last_name = BRANCHE
        gender = 2
        mail_type = 1
        title = "firma"
        data_list.append([name,
                          email,
                          street,
                          number + number_encoded,
                          postal_code,
                          city,
                          website,
                          gender,
                          last_name,
                          mail_type,
                          title])
        customer_progress.update(i)
        # print "saved "+str(SAVED_COSTUMERS + 1) + ". Customer data_list"
    customer_progress.finish()
    print
    return data_list


def write_to_csv(data_list):
    """writing data to csv"""
    print "writing CSV"
    # print "saving " + str(SAVED_COSTUMERS + 1) + ". Customer."
    with open(BRANCHE + str(time.time()) + ".csv", "w") as leads_csv:
        writer = csv.writer(leads_csv, delimiter=";")
        #try:
        i=0
        for customer in data_list:
            widget_csv_progress = [Bar('>'), ' ', 'CSV writing', ' ', ReverseBar('<')]
            csv_progress = ProgressBar(widgets=widget_csv_progress, maxval=len(data_list)).start()
            writer.writerow(customer)
            csv_progress.update(i)
            i += 1
        csv_progress.finish()
        #except TypeError:
            #print data_list[i]
            #print("Error TypeError.. data_list len: " + str(len(data_list[0])))
    print "writen"

    return 0


def b64_encode(suffix):
    """encode"""
    encode = base64.b64decode(suffix)

    return encode


def main():
    """main entry point"""
    number = find_results(URL)  # ukupno rezultata
    pages = create_pages(number)  # generates all pages to scrap...example 2 pages with 30 Customers together
    number_of_pages = len(pages)
    # print number_of_pages
    customer_data = []
    for i in range(number_of_pages):
        widgets_o_p = [Bar('>'), ' ', str(i + 1) + ' Pages Done:', ' ', ReverseBar('<')]
        overal_progress = ProgressBar(widgets=widgets_o_p, maxval=number_of_pages).start()
        customers_on_page = get_customers(pages[i])
        overal_progress.update(i)
        """get links from page[i]"""
        write_to_csv(get_data_from(customers_on_page))
        """get data for every Customer on page [i]"""
        # print customer_data
    overal_progress.finish()

    return


if __name__ == '__main__':
    sys.exit(main())
